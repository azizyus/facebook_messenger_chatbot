<?php

/**
 * Created by PhpStorm.
 * User: Aziz
 * Date: 19.08.2017
 * Time: 08:10
 */
namespace App;

class Buttons
{


    #buttons

    public static $buttonTemplate;


    function __construct()
    {
    }


    public static function addCallButton($title,$number)
    {

        self::$buttonTemplate["data"]["buttons"][]=[


            "type"=>"phone_number",
            "title"=>$title,
            "payload"=>$number


        ];

        return new static;


    }

    public static function addLinkButton($title,$url)
    {


        self::$buttonTemplate["data"]["buttons"][]=[

            "type"=>"web_url",
            "title"=>$title,
            "url"=>$url,


        ];



        return new static;

    }

    public static function addShareButton()
    {


        self::$buttonTemplate["data"]["buttons"][]=[

            "type"=>"element_share"

        ];

        return new static;

    }

    public static function addPostbackButton($title,$payload)
    {

        self::$buttonTemplate["data"]["buttons"][]=[

            "title"=>$title,
            "type"=>"postback",
            "payload"=>$payload

        ];

        return new static;

    }



    public static function title($title)
    {

        self::$buttonTemplate["data"]["title"]=$title;
        self::$buttonTemplate["data"]["type"]="button";

        return new static;

    }

    public static function create()
    {
        self::$buttonTemplate["type"]="button";

        $buttons = self::$buttonTemplate;
        self::$buttonTemplate=[];

        return $buttons;

    }


}