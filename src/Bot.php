<?php

/**
 * Created by PhpStorm.
 * User: Aziz
 * Date: 19.08.2017
 * Time: 02:54
 */

namespace App;



class Bot
{




    public static $access_token;
    public static $user_id;
    public static $message;
    public static $data;
    public static $options;
    public static $domain;
    public static $payload;

    public static function setAccessToken($access_token)
    {
        self::$access_token=$access_token;
    }


    function __construct()
    {



        self::$domain="https://messenger.diffea.com/";

        self::listen();


        function reply($param)
        {


            $data["data"]=$param["data"];
            $data["type"]=$param["type"];

            return $data;

        }



//reply




    }


    public static function getUserId()
    {
        return self::$user_id;
    }

    public static function stringHasPlaceHolder($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }




    public static function typeSelector($param,$comparator,$callback)
    {


        //$param -> benim bekliediği
        //$comparator -> messenger üzerinden gelen veri

        //hears --> message
        //postback --> payload





        #PLACEHOLDERS

        //heard(" %veri% ") şeklinde bir ifade var ise bunu ayırır ve anonim fonksiyon içine parametre olarak basar
        $functionParams=[];
        $pass=false;
        //boştan farklıysa place holder var demektir
        if(self::stringHasPlaceHolder($param,"%","%")!="")
        {

            $input = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $comparator)));
            $string = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ",$param)));

            $stringArray = explode(" ",$string);
            $inputArray = explode(" ",$input);

            if(!(count($stringArray)==count($inputArray))) return "";


            for ($i=0; $i < count($stringArray); $i++) {
                $x = $stringArray[$i];
                $parsed= self::stringHasPlaceHolder($x,"%","%");
                if($parsed!="")
                {
                    $variables[]=$parsed;
                    $places[]=$i;
                }

            }

            foreach ($places as $p) {
                $myData[] = $inputArray[$p];
                //${$inputArray[$p]}

            }

            for ($i=0; $i <count($myData) ; $i++) {

                //${$variables[$i]} = $myData[$i];

                $functionParams[$variables[$i]]=$myData[$i];

            }

            $pass=true;

        }
        #END PLACEHOLDERS



        if($param==$comparator || $pass)
        {
            $reply  =  call_user_func_array($callback,$functionParams);





          if($reply!=NULL)  self::prepareTypes($reply);

        }

    }

    public static function multipReply($reply)
    {

        self::prepareTypes($reply);

    }

    static function prepareTypes($reply)
    {

        //sadece düz text cevaplar için
        if($reply["type"]=="text")  $data = self::prepareText($reply);

        //chate link ile resim yükleme
        elseif($reply["type"]=="imageLink") $data = self::prepareImageLink($reply);

        //chate link ile video yükleme
        elseif($reply["type"]=="videoLink") $data = self::prepareVideoLink($reply);

        //chate link ile dosya yükleme
        elseif($reply["type"]=="fileLink") $data = self::prepareFileLink($reply);


        //chate link ile dosya yükleme
        elseif($reply["type"]=="audioLink") $data = self::prepareAudioLink($reply);




        //DİSKTEN RESİM YÜKLEME AMA BİTMEDİ
        elseif($reply["type"]=="imageUpload") $data = self::prepareImageUpload($reply);



        #buttons

        elseif($reply["type"]=="button") $data = self::prepareButtons($reply);

        elseif($reply["type"]=="genericTemplate") $data = self::prepareGeneric($reply);


        //list template aslında generic item listesi
        elseif($reply["type"]=="listTemplate") $data = self::prepareList($reply);



        elseif($reply["type"]=="quickReply") $data = self::prepareQuickReply($reply);

        #end buttons



        //data boş değilse
        if(isset($data) && $data!="")  self::executeReply($data);
        else //data boş ise
        {
            $reply["data"]="HATA (check return reply(), type is null)";
            $reply["type"]="text";
            $data = self::prepareText($reply);
            self::executeReply($data);
        }

    }



    public static function hears($param,$callback)
    {


        self::typeSelector($param,self::$message,$callback);

    }

    public static function postback($param,$callback)
    {
        self::typeSelector($param,self::$payload,$callback);
    }


    public function listen()
    {
        $request = file_get_contents("php://input");

        $request = json_decode($request);
        self::$user_id= $request->entry[0]->messaging[0]->sender->id;
        self::$message = $request->entry[0]->messaging[0]->message->text;


        if(isset($request->entry[0]->messaging[0]->postback->payload))
        {
//            file_put_contents("t.txt",self::$user_id);
            self::$payload=$request->entry[0]->messaging[0]->postback->payload;
            return "payload_request";
        }
        elseif(isset($request->entry[0]->messaging[0]->message->quick_reply->payload))
        {

            self::$payload=$request->entry[0]->messaging[0]->message->quick_reply->payload;
            return "payload_request";

        }
        else return "normal_request";




    }






    //chate diskten  resim yükleme BİTMEDİ
    public static function prepareImageUpload($message)
    {


        $user_id=self::$user_id;

        $data = array(

            'recipient' =>  array('id'=>"$user_id"),
            'message' => [

                "attachment"=>[


                    "type"=>"image",
                    "payload"=>[


                        "url"=>""

                    ]


                ],
                'filedata'=>file_get_contents($message["path_to_image"])

            ]


        );


        return $data;


    }




    #generic


    public static function prepareList($message)
    {



        $user_id = self::$user_id;




        $data = array(

            'recipient' =>  array('id'=>"$user_id"),
            'message' => [

                "attachment"=>[


                    "type"=>"template",
                    "payload"=>[

                        "template_type"=>"list",
                        "top_element_style" => $message["data"]["top_element_style"]==NULL ? "compact" : "large",
                        "elements"=>$message["data"]["elements"],
                        "buttons"=> $message["data"]["buttons"]





                    ]


                ]

            ]


        );



        return $data;
    }



    public static function prepareGeneric($message)
    {
        $user_id = self::$user_id;



        $data = array(

            'recipient' =>  array('id'=>"$user_id"),
            'message' => [

                "attachment"=>[


                    "type"=>"template",
                    "payload"=>[

                        "template_type"=>"generic",
                        "elements"=>[

                            $message["data"],

                        ]








                    ]


                ]

            ]


        );



        return $data;
    }

    #end generic


    #buttons
    public static function prepareButtons($message)
    {




        $user_id = self::$user_id;
        $data = array(

            'recipient' =>  array('id'=>"$user_id"),
            'message' => [

                "attachment"=>[


                    "type"=>"template",
                    "payload"=>[


                        "template_type"=>$message["data"]["type"],
                        "text"=>$message["data"]["title"],
                        "buttons"=>$message["data"]["buttons"]

                    ]


                ]

            ]


        );



        return $data;
    }

    public static function prepareQuickReply($message)
    {

        $user_id = self::$user_id;
        $data = [

            'recipient' =>  array('id'=>"$user_id"),
            'message' => [

                "text"=>$message["data"]["title"],
                "quick_replies"=> $message["data"]["buttons"]

            ]


        ];



        return $data;

    }

    #end buttons

    #links

    //chate link ile dosya yükleme
    public static function prepareAudioLink($message)
    {


        $user_id=self::$user_id;

        $data = array(

            'recipient' =>  array('id'=>"$user_id"),
            'message' => [

                "attachment"=>[


                    "type"=>"audio",
                    "payload"=>[


                        "url"=>self::$domain.$message["data"]

                    ]


                ]

            ]


        );


        return $data;


    }


    //chate link ile dosya yükleme
    public static function prepareFileLink($message)
    {


        $user_id=self::$user_id;

        $data = array(

            'recipient' =>  array('id'=>"$user_id"),
            'message' => [

                "attachment"=>[


                    "type"=>"file",
                    "payload"=>[


                        "url"=>self::$domain.$message["data"]

                    ]


                ]

            ]


        );


        return $data;


    }


    //chate link ile video yükleme
    public static function prepareVideoLink($message)
    {


        $user_id=self::$user_id;

        $data = array(

            'recipient' =>  array('id'=>"$user_id"),
            'message' => [

                "attachment"=>[


                    "type"=>"video",
                    "payload"=>[


                        "url"=>self::$domain.$message["data"]

                    ]


                ]

            ]


        );


        return $data;


    }


    //chate link ile resim yükleme
    public static function prepareImageLink($message)
    {


        $user_id=self::$user_id;

        $data = array(

            'recipient' =>  array('id'=>"$user_id"),
            'message' => [

                "attachment"=>[


                    "type"=>"image",
                    "payload"=>[


                        "url"=>self::$domain.$message["data"]

                    ]


                ]

            ]


        );


        return $data;


    }

    #end links




    //sadece düz text cevaplar için
    public static function prepareText($message)
    {
        $user_id=self::$user_id;

            $data = array(

                'recipient' =>  array('id'=>"$user_id"),
                'message' => array('text'=>"".$message["data"]."")


            );


            return $data;



    }


    #GET STARTED BUTTON
    public static function deleteGetStartedButton()
    {

        $data["url"]="https://graph.facebook.com/v2.6/me/messenger_profile?access_token=";

        $data["data"]=[

            "fields"=>[

                "get_started"

            ]

        ];

        return $data;

    }

    public static function createGetStartedButton($payload="GET_STARTED_PAYLOAD")
    {

        $data["url"]="https://graph.facebook.com/v2.6/me/messenger_profile?fields=get_started&access_token=";

        $data["data"]=[

            "get_started"=>[

                "payload"=>$payload

            ]

        ];




        return $data;

    }
    #END GET STARTED BUTTON


    #GREETING TEXT

    #ESKİ
    public static function setGreetingTextThread($text)
    {

        $data["url"]="https://graph.facebook.com/v2.6/me/thread_settings?access_token=";

        $data["data"]=[

            "setting_type"=>"greeting",
            "greeting"=>[
                "text"=>$text
            ]

        ];



        return $data;


    }
    public static function deleteGreetingTextThread()
    {

        $data["url"]="https://graph.facebook.com/v2.6/me/thread_settings?access_token=";

        $data["data"]=[

            "setting_type"=>"greeting",
        ];



        return $data;


    }
    #END ESKİ

    #YENİ
    public static function setGreetingText($locale,$text)
    {

        $data["url"]="https://graph.facebook.com/v2.6/me/messenger_profile?access_token=";

        $data["data"]=[
            "greeting"=>[

                [
                    "locale"=>$locale,
                    "text"=>$text
                ]

            ]

        ];



        return $data;


    }
    public static function deleteGreetingText()
    {

        $data["url"]="https://graph.facebook.com/v2.6/me/messenger_profile?access_token=";

        $data["data"]=[

            "fields"=>[
                "greeting"
            ]

        ];

        return $data;

    }
    #END YENİ

    #END GREETING TEXT




    #custom_request -> POST,DELETE,GET,PUT, $data will be encoded by json if encode param is true, default encode param is true
    static public function executeProfileSettings($custom_request,$data,$encode=true)
    {
        if($encode)  $json = json_encode($data["data"]);
        $ch = curl_init($data["url"].self::$access_token);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $custom_request);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;

    }


    #JUST TAKES DATA -> CONVERT TO JSON ->  POST TO URL
    static public function executeReply($data,$encode=true)
   {

       if($encode)$data=json_encode($data);

       $ch = curl_init('https://graph.facebook.com/v2.6/me/messages?access_token='.self::$access_token);
       curl_setopt($ch, CURLOPT_POST, 1);
       curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
       curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);
       $result = curl_exec($ch);
       curl_close($ch);

       file_put_contents("t.txt",$data);

       return $result;

       exit;


   }




}

