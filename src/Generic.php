<?php

/**
 * Created by PhpStorm.
 * User: Aziz
 * Date: 19.08.2017
 * Time: 09:28
 */

namespace App;

class Generic
{



    public static $genericTemplate;


    function __construct()
    {

    }

    public static function template($title,$image_url,$subtitle)
    {


        self::$genericTemplate["data"]=[


            "title"=>$title,
            "image_url"=>$image_url,
            "subtitle"=>$subtitle

        ];



        return new static;

    }

    public static function defaultAction($url)
    {

        self::$genericTemplate["data"]["default_action"]=[

            "type"=>"web_url",
            "url"=>$url,
            "webview_height_ratio"=>"tall",




        ];



        return new static;
    }





    public static function buttons($buttons)
    {

        self::$genericTemplate["data"]["buttons"]=$buttons["data"]["buttons"];

        return new static;
    }

    public static function button($button)
    {


        self::$genericTemplate["data"]["buttons"]=$button["data"]["buttons"];

        return new static;

    }

    public static function create()
    {


        self::$genericTemplate["type"]="genericTemplate";


        return self::$genericTemplate;


    }


}