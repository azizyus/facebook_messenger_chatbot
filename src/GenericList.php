<?php

/**
 * Created by PhpStorm.
 * User: Aziz
 * Date: 20.08.2017
 * Time: 07:07
 */
namespace App;

class GenericList
{




    function __construct()
    {
    }


    public static $template;




    public static function cover()
    {
        self::$template["data"]["top_element_style"]="large";

        return new static;
    }

    public static function item($item)
    {


        self::$template["data"]["elements"][]=$item["data"];

        return new static;

    }

  public static function button($button)
  {


      self::$template["data"]["buttons"]=$button["data"]["buttons"];

      return new static;

  }


    public static function create()
    {

        self::$template["type"]="listTemplate";

        return self::$template;

    }



}