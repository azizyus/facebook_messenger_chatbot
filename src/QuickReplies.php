<?php

/**
 * Created by PhpStorm.
 * User: Aziz
 * Date: 19.08.2017
 * Time: 08:10
 */

namespace App;
class QuickReplies
{


    #buttons

    public static $quickReplyTemplate;



    public static function addQuickReply($title,$payload,$image_url=NULL)
    {

        self::$quickReplyTemplate["data"]["buttons"][]=[

          "content_type"=>"text",
          "title"=>$title,
            "payload"=>$payload,
        ];


        if($image_url!=NULL)
        {
            $count = count(self::$quickReplyTemplate["data"]["buttons"]);
            self::$quickReplyTemplate["data"]["buttons"][$count-1]["image_url"]=$image_url;
        }

        return new static;

    }

    public static function addQuickReplyLocation($title,$payload,$image_url=NULL)
    {
        self::$quickReplyTemplate["data"]["buttons"][]=[

            "content_type"=>"location",
            "title"=>$title,
            "payload"=>$payload,
        ];


        if($image_url!=NULL)
        {
            $count = count(self::$quickReplyTemplate["data"]["buttons"]);
            self::$quickReplyTemplate["data"]["buttons"][$count-1]["image_url"]=$image_url;
        }

        return new static;

    }

    public static function title($title)
    {


        self::$quickReplyTemplate["data"]["title"]=$title;

        return new static;

    }

   

    public static function create()
    {
        self::$quickReplyTemplate["type"]="quickReply";
        return self::$quickReplyTemplate;

    }


}